package com.xnsio.cleancode;

import org.junit.Assert;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.Month;

public class MeetingCalendarTest {

    private MeetingCalendar meetingCalendarParticipantOne;
    private MeetingCalendar meetingCalendarParticipantTwo;
    public static final LocalDateTime LOCAL_DATE_TIME_11AM = LocalDateTime.of(2019, Month.NOVEMBER, 7, 11, 00);

    @Test
    public void commonMeetingSlot() {
        forParticipantsWithFreeSlotAtElevenAM().returnFreeSlot();
    }

    private MeetingCalendarTest forParticipantsWithFreeSlotAtElevenAM() {
        meetingCalendarParticipantOne = new MeetingCalendar(new Name("F1","L1"), LOCAL_DATE_TIME_11AM);
        meetingCalendarParticipantTwo = new MeetingCalendar(new Name("F2","L2"), LOCAL_DATE_TIME_11AM);
        return this;
    }

    private void returnFreeSlot() {
        Assert.assertEquals(LOCAL_DATE_TIME_11AM, meetingCalendarParticipantOne.contains(meetingCalendarParticipantTwo));
    }
}
