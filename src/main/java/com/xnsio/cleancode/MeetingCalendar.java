package com.xnsio.cleancode;

import java.time.LocalDateTime;

public class MeetingCalendar {
    Name name;
    LocalDateTime localDateTime;

    public MeetingCalendar(Name name, LocalDateTime localDateTime) {
        this.name = name;
        this.localDateTime = localDateTime;
    }

    public LocalDateTime contains(MeetingCalendar meetingCalendar) {
        if(this.name == meetingCalendar.name)
            return null; 
        if(this.localDateTime.compareTo(meetingCalendar.localDateTime) == 0) {
            return meetingCalendar.localDateTime;
        }
        return null;
    }
    
}
